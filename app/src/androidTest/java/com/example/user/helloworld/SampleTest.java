package com.example.user.helloworld;

import android.app.Activity;

import com.example.user.helloworld.MainActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Juraj on 13.2.2015..
 */

@RunWith(RobolectricGradleTestRunner.class)
public class SampleTest {

    @Test
    public void testRobolectricWorks() throws Exception {
        Activity activity = Robolectric.buildActivity(MainActivity.class).create().get();
        assertNotNull(activity);
    }
}
